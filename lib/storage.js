import localforage from 'localforage'

localforage.config()
const db = localforage.createInstance({ name: 'db' })

function sortWatchlist(watchlist) {
  watchlist = watchlist.sort((e1, e2) => e1.ticker.localeCompare(e2.ticker))
  return watchlist
}
async function getWatchlist() {
  let watchlist = await db.getItem('watchlist')
  if (!watchlist) {
    watchlist = []
    await db.setItem('watchlist', watchlist)
  }
  return watchlist
}

async function isInWatchlist(ticker, name) {
  let inWatchlist = false
  const watchlist = await getWatchlist()
  watchlist.forEach(function(el) {
    if (el.ticker === ticker && el.name === name) {
      inWatchlist = true
    }
  })
  return inWatchlist
}

async function addToWatchlist(ticker, name) {
  if (await isInWatchlist(ticker, name)) {
    return
  }
  let watchlist = await getWatchlist()
  watchlist.push({ ticker, name })
  watchlist = sortWatchlist(watchlist)
  await db.setItem('watchlist', watchlist)
}

async function removeFromWatchlist(ticker, name) {
  if (!(await isInWatchlist(ticker, name))) {
    return
  }
  let watchlist = await getWatchlist()
  watchlist = watchlist.filter(
    (item) => item.name !== name && item.ticker !== ticker
  )
  await db.setItem('watchlist', watchlist)
}

export { db, getWatchlist, isInWatchlist, addToWatchlist, removeFromWatchlist }
