import axios from 'axios'
import { db } from '@/lib/storage'

const TOKEN = 'bq2cguvrh5rc5ioof81g'
// refresh quote every 15 minutes max
const QUOTE_DELAY = 60 * 15 * 1000

const request = axios.create({
  // workaround CORS restriction
  baseURL: 'https://cors-anywhere.herokuapp.com/https://finnhub.io/api/v1',
  crossDomain: true,
  headers: {
    Accept: 'application/json',
    'X-Request-With': 'bowo'
  }
})

const api = {
  async getStocksList() {
    // FIXME: cache stock list for a limited time
    let data = await db.getItem('stocksList')
    if (!data || !data.length) {
      const res = await request.get(`/stock/symbol?exchange=US&token=${TOKEN}`)
      data = res.data
    }
    await db.setItem('stocksList', data)
    return data
  },

  async searchStocks(query, max = 20) {
    query = query.toLowerCase()
    if (!query) {
      return []
    }
    const stockList = await this.getStocksList()
    let data = stockList.filter((el) => {
      return (
        el.description.toLowerCase().startsWith(query) ||
        el.symbol.toLowerCase().startsWith(query)
      )
    })
    data = data.slice(0, max)

    data = data.map((el) => {
      return { ticker: el.symbol, name: el.description.toLowerCase() }
    })
    return data
  },

  async getStockQuote(ticker) {
    const timestamp = Date.now()
    let data = await db.getItem(`stockQuote.${ticker}`)
    // check if no data or outdated data
    if (
      !data ||
      !data.price ||
      !data.change ||
      timestamp - data.timestamp >= QUOTE_DELAY
    ) {
      const res = await request.get(`/quote?symbol=${ticker}&token=${TOKEN}`)
      data = {
        price: res.data.c,
        // percent change: (current - open) / current * 100
        change: ((res.data.c - res.data.o) / res.data.c) * 100,
        timestamp
      }
      await db.setItem(`stockQuote.${ticker}`, data)
    }
    return data
  }
}

export default api
